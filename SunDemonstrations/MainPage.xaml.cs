﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using SunGraphicsX;
using Windows.UI;
// “空白页”项模板在 http://go.microsoft.com/fwlink/?LinkId=234238 上有介绍

namespace SunDemonstrations
{
    /// <summary>
    /// 可用于自身或导航至 Frame 内部的空白页。
    /// </summary>
    public sealed partial class MainPage : Page
    {
        Image ScreenImage;      
        Demonstration TheDemonstration;


        public MainPage()
        {
            this.InitializeComponent();

            //创建充当屏幕的图片
            ScreenImage = new Image();
            ScreenImage.HorizontalAlignment = HorizontalAlignment.Left;
            ScreenImage.VerticalAlignment = VerticalAlignment.Top;
            contentGrid.Children.Add(ScreenImage);

        }

        //TODO:使用combo list简化以下的代码
        private void BlueDemo_Click(object sender, RoutedEventArgs e)
        {
            TheDemonstration = new BlueDemo(ScreenImage, new Size(100,200));
            TheDemonstration.DemonstrateTheScene();
            
        }

        private void RedDemo_Click(object sender, RoutedEventArgs e)
        {
            TheDemonstration = new RedDemo(ScreenImage, new Size(100, 300));
            TheDemonstration.DemonstrateTheScene();
        }

        private void contentGrid_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (TheDemonstration == null)
                return;

            //TODO：设置Demostration对输入的响应
          
        }



    }
}
