﻿using Windows.Foundation;
using Windows.UI;
using Windows.UI.Xaml.Controls;
namespace SunDemonstrations
{
    class BlueDemo : Demonstration
    {
        public BlueDemo(Image image, Size size)
            : base(image, size)
        {

        }

        public override void DemonstrateTheScene()
        {
            //base.DemonstrateTheScene();

            Graphics.PaintScreen(Colors.Blue);
        }
    }
}
