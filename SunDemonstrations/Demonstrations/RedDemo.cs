﻿using Windows.Foundation;
using Windows.UI;
using Windows.UI.Xaml.Controls;
namespace SunDemonstrations
{
    class RedDemo : Demonstration
    {
        public RedDemo(Image image, Size size)
            : base(image, size)
        {

        }

        public override void DemonstrateTheScene()
        {
            //base.DemonstrateTheScene();

            Graphics.PaintScreen(Colors.Red);
        }
    }
}
