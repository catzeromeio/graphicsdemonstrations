﻿using SunGraphicsX;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Xaml.Controls;
namespace SunDemonstrations
{
    public class Demonstration
    {

        protected SunGraphics Graphics;
        
        public Demonstration(Image image, Size size)
        {
            Graphics = new SunGraphics(image, size);
        }

        public virtual void DemonstrateTheScene()
        {
            Graphics.PaintScreen(Colors.White);
        }

        public void ChangeSize(Size size)
        {
            Graphics.SetScreenImageSize(size);
        }
    }
}