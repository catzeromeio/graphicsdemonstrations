﻿using System.IO;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.UI;
using Windows.Foundation;

namespace SunGraphicsX
{
    public class SunGraphics
    {
        Image ScreenImage;
        WriteableBitmap ScreenBitmap;
        Stream PixelsStream;
        byte[] Pixels; 

        public SunGraphics(Image image, Size size)
        {
            ScreenImage = image;
            SetScreenImageSize(size);            
        }

        public void SetScreenImageSize(Size size)
        {
            ScreenBitmap = new WriteableBitmap((int)size.Width, (int)size.Height);
            Pixels = new byte[4 * ScreenBitmap.PixelWidth * ScreenBitmap.PixelHeight];
            PixelsStream = ScreenBitmap.PixelBuffer.AsStream();           
            ScreenImage.Source = ScreenBitmap;
        }

        public void PaintScreen(Color color)
        {
            int index = 0;
            for (int y = 0; y < ScreenBitmap.PixelHeight; ++y)
            {
                for (int x = 0; x < ScreenBitmap.PixelWidth; ++x)
                {
                    Pixels[index++] = color.B;
                    Pixels[index++] = color.G;
                    Pixels[index++] = color.R;
                    Pixels[index++] = color.A;
                }
            }

            PixelsStream.Write(Pixels,0,Pixels.Length);
            PixelsStream.Seek(0,SeekOrigin.Begin);
            ScreenBitmap.Invalidate();
        }

        public void DrawDot(Point point, Color color)
        {
            int index = ((int)point.Y)*ScreenBitmap.PixelWidth*4 + ((int)point.X)*4;

            byte[] pixels = new byte[4] {color.B,color.G,color.R,color.A};

            PixelsStream.Seek(index,SeekOrigin.Begin);
            PixelsStream.Write(pixels, 0, pixels.Length);
            ScreenBitmap.Invalidate();
            PixelsStream.Seek(0, SeekOrigin.Begin);
        }
    }
}